import datetime
import json
import os
import shutil
import subprocess
import uuid
import zipfile
from datetime import datetime, timedelta
from pathlib import Path as CPath
from typing import Dict, List
from zipfile import ZipFile

from fastapi import FastAPI, File, Form, Request, UploadFile
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import FileResponse, JSONResponse, StreamingResponse
from fastapi.staticfiles import StaticFiles
from minio import Minio
from pydantic import BaseModel

from .image_processor import ImageProcessor, OpStates, makedirp

NOW = datetime.now().date()
mc = Minio(
    os.environ.get("AWS_ENDPOINT_URL", "").replace("http://", ""),
    os.environ.get("AWS_ACCESS_KEY_ID", ""),
    os.environ.get("AWS_SECRET_ACCESS_KEY", ""),
    secure=False,
)


app = FastAPI()

app.mount("/downloads", StaticFiles(directory="/", html=True), name="static")


origins = ["*"]

max_attempts = int(os.environ.get("MAX_ATTEMPTS", 5))

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


def run(x):
    subprocess.run(x, shell=True)


class Path(BaseModel):
    path: str


class UploadData(BaseModel):
    partner: str
    process: str
    date: str = None
    file: UploadFile = File(...)


class InfluxQueryData(BaseModel):
    process: str
    timestamp: str
    duration: int


@app.get("/liveness")
def liveness():
    return "running"


@app.post("/process/{file_path:path}")
async def process_file(file_path: str) -> str:
    """
    Process the uploaded SVS files:

    Arguments:
        - path (str): The path of the SVS files .zip file.
    Returns:
        - output (str): The path of the created .zip with the stitched outputs.
    """
    path = "/tmp/histo" / CPath(file_path)
    print("Processing")

    pr = ImageProcessor(path)
    uid = file_path.split('/')[0]
    # pr.to_tiff('backend/output/raw/' + o + '.tif')
    makedirp('.slides')
    makedirp(f'output/masks/{uid}')
    makedirp(f'output/raw/{uid}')

    with zipfile.ZipFile(path, 'r') as zip_ref:
        zip_ref.extractall(path.parent)

    for p in list(CPath(path).glob('.svs')):
        mask = pr.get_mask()
        attempts = 0
    # unzip files
        while pr.create_mask_tiff(mask, "backend/output/masks/" + str(p) +
                                  ".tif") == OpStates.FAILED and attempts < max_attempts:
            attempts += 1
    pr.clean()

    uuid.uuid1()


@app.post("/upload/{file_path:path}")
async def create_upload_file(file_path: str, file: Request):
    uid = uuid.uuid1()
    file_path = str(uid) / CPath(file_path)
    path = "/tmp/histo" / file_path
    print("[INFO] Uploading to " + file_path)
    if not os.path.exists(path.parent):
        os.makedirs(path.parent)

    with open(path, "wb") as fp:
        async for b in file.stream():
            print("Chunk")
            fp.write(b)
    print("[INFO] Pushing")
    mc.fput_object("projects", "/diss-markus-bauer/histo/input" + file_path, str(path))
    print("[INFO] Removing buffer")
    # shutil.rmtree(path.parent)
    return str(file_path)


@app.post("/download", response_class=StreamingResponse, status_code=200)
async def files(path: Path):
    path = CPath(path.path)
    obj = mc.get_object(
        "projects/diss-markus-bauer/histo/processed", str(path)
    ).stream()
    return StreamingResponse(obj, media_type="application/octet-stream")
