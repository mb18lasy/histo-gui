import json
import os
import sys
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from PIL import Image as PImage
from pyvips import Image
from scipy.ndimage import binary_closing, find_objects, label, median_filter

sys.path.insert(0, "../deps/pyhist")  # noqa
sys.path.insert(0, "deps/pyhist")  # noqa
sys.path.insert(0, "deps/pyhist/src/graph_segmentation/segment")  # noqa
sys.path.insert(0, "../deps/pyhist/src/graph_segmentation/segment")  # noqa
from src.slide import PySlide, TileGenerator  # pyright: ignore # noqa
from src.utility_functions import clean  # pyright: ignore # noqa

SAMPLING_RATIO = (
    32  # the ratio to reduce image for mask creation (reverted in final TIFF)
)


class MaskGenerator:
    def __init__(self, input_slide: PySlide):
        self.tilegen = TileGenerator(input_slide)

    def execute(self) -> np.ndarray:
        """Executes a mask generation process."""
        if self.tilegen.method == "graph":
            return self.tilegen._TileGenerator__graph()
        if self.tilegen.method == "otsu":
            return self.tilegen._TileGenerator__otsu()
        return self.tilegen._TileGenerator__adaptive()


class ImageProcessor:
    def __init__(self, path):
        self.slide = PySlide(
            {
                "svs": path,
                "output": ".slides",
                "method": "adaptive",
                "mask_downsample": SAMPLING_RATIO,
                "sigma": 0.5,
                "k_const": 10000,
                "minimum_segmentsize": 10000,
                "pct_bc": 5,
                "borders": "1111",
                "corners": "0000",
                "save_mask": False,
                "save_edges": False,
            }
        )
        self.resolution = self.slide.slide.properties["aperio.MPP"]
        self.mag = self.slide.slide.properties["aperio.AppMag"]
        self.path = path

    def to_tiff(self, outpath: str):
        """
        Convert the SVS input to a bigtiff.

        Arguments:
            outpath (str): the path to store the output to
        """
        img = Image.new_from_file(self.path)
        img = img.thumbnail_image(
            int(img.width / SAMPLING_RATIO), height=int(img.height / SAMPLING_RATIO)
        )  # pyright: ignore
        img = img.thumbnail_image(
            SAMPLING_RATIO * int(img.width) + 1,
            height=SAMPLING_RATIO * int(img.height) + 1,
        )  # pyright: ignore
        img.tiffsave(
            outpath,
            compression="lzw",
            bigtiff=True,
            tile=True,
            tile_width=512,
            tile_height=512,
            pyramid=True,
        )  # pyright: ignore
        del img

    def get_mask(self) -> np.ndarray:
        """
        Segment the foreground and store the result as bigtiff.

        Returns:
            mask (np.ndarray): A mask with 0 (background) and 255 (tissue) as greyscale image.
        """
        generator = MaskGenerator(self.slide)
        mask, color = generator.execute()
        r, g, b = color
        color = 0.2989 * r + 0.5870 * g + 0.1140 * b
        mask = np.array(mask) < color
        mask = mask.astype("int16")
        mask = binary_closing(mask, iterations=8).astype("int16")
        mask = median_filter(mask, size=20).astype("int16")
        # Filter out smaller elements
        objs = sorted(
            find_objects(label(mask)[0]), key=lambda x: len(mask[x].flatten())
        )[::-1][1:]
        for obj in objs:
            mask[obj] = 0
        return mask.astype("uint8") * 255

    def create_mask_tiff(self, mask: np.ndarray, outpath: str):
        """
        Sample up the created mask and store it as pyramidal tiff file.

        Arguments:
            - mask (np.ndarray): the created mask
            - outpath (str): the path to store the output
        """

        img = Image.new_from_array(mask)
        # img = img.colourspace("b-w")
        dims = self.slide.slide.level_dimensions[0]
        img = img.thumbnail_image(dims[0], height=dims[1])  # pyright: ignore
        # img = img.affine((SAMPLING_RATIO, 0, 0, SAMPLING_RATIO))  # pyright: ignore
        img.tiffsave(
            outpath,
            compression="lzw",
            bigtiff=True,
            tile=True,
            tile_width=512,
            tile_height=512,
            pyramid=True,
        )  # pyright: ignore

    def clean(self):
        clean(self.slide)


def main():
    paths = Path("data/Pathologie UKBonn").glob("*.svs")
    paths = list(paths)
    with open("./mapping.json", "r") as fp:
        mapping = json.load(fp)
    included = [mapping[x] for x in "NOLMTURS"]
    paths = list(filter(lambda x: str(x) in included, paths))
    print(paths)

    def makedirp(path):
        parts = path.split("/")

        for p in parts[1:]:
            path += p + "/"
            if os.path.exists(path):
                os.makedirs(path)

    makedirp("backend/output/masks")
    makedirp("backend/output/raw")
    makedirp(".slide")

    for p in paths:
        print(p)
        pr = ImageProcessor(p)
        # pr.to_tiff('backend/output/raw/' + str(p).split('/')[-1].replace('.svs', '.tif'))
        mask = pr.get_mask()
        pr.create_mask_tiff(
            mask,
            "backend/output/masks/" + str(p).split("/")[-1].replace(".svs", ".tif"),
        )
        pr.clean()


if __name__ == "__main__":
    main()
