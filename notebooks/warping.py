# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.14.4
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# !bat ../mapping.json

import openslide as os
from PIL import Image
from typing import Union
import numpy as np
import matplotlib.pyplot as plt
import cv2
from skimage import color, morphology
import torchgeometry as tgm
import torch
from typing import Tuple, Union
from scipy.interpolate import LinearNDInterpolator
from fastai.vision.all import Learner, DataLoaders
from segmentation_models_pytorch import Unet
from elasticdeform.torch import deform_grid as tdeform_grid
from elasticdeform import deform_grid 
from fastai.callback.all import SaveModelCallback
import gc
#from elasticdeform import deform_grid


# First, we Load a WSI and split it into two parts. We then generate a random polynom, two simulate warping.

# +
slide = os.OpenSlide("../data/Pathologie UKBonn/ac0cc261-2ba8-ae3d-25b8-06136b14b732_085223.svs")
levels = slide.level_dimensions
img = slide.read_region(levels[3], 3, levels[3]).convert("L")
width, height = img.width, img.height
nwidth = int(0.95*width)
nheight = int(0.95*height)

img = img.crop((width-nwidth, height-nheight, nwidth, nheight)).convert("L")

# -

img


# Converting image to a binary image 
# ( black and white only image). 
def get_countours(img: Image):
    _, threshold = cv2.threshold(np.asarray(img).astype('uint8'), 240, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU) 
    
    # Detecting contours in image. 
    footprint = morphology.disk(1)
    
    #for i in range(10):
    threshold = morphology.remove_small_objects(threshold > 0, min_size=40050)
    threshold = morphology.remove_small_holes(threshold > 0, 100000)
    
    
    contours, _= cv2.findContours(threshold.astype('uint8'), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    
    return np.vstack(contours).reshape(-1, 2), threshold

# +
plt.figure()
contours, threshold = get_countours(img)

plt.imshow(np.asarray(img)*(threshold.astype(int)), cmap="gray")

plt.scatter(contours[:,0], contours[:,1], color="r")


# +
def create_splited_wsi(slide: Union[os.OpenSlide, Image.Image]):
    levels = slide.level_dimensions
    img = slide.read_region(levels[3], 3, levels[3]).convert("L")
    width, height = img.width, img.height
    nwidth = int(0.95*width)
    nheight = int(0.95*height)
    
    img = img.crop((width-nwidth, height-nheight, nwidth, nheight))
    arr = np.array(img)
    contours, threshold = get_countours(img.convert("L"))
    x = contours[:,0]
    y = contours[:,1]
    #img = np.array(arr[min(y):max(y), min(x):max(x), :])
    img = np.array(img)
    w, h = img.shape

    left = img[:,:h//2]
    right = img[:,h//2:]
    return left, right

def get_transform_coordinates(left: np.array, right: np.array, plot=False): 
    left_contours, _ = get_countours(left)
    right_contours, _ = get_countours(right)
    
    def params():
        return [{"freq": np.random.choice(np.linspace(0.5, 4.5, 100)), "amp": 0.1, },
                {"freq": np.random.choice(np.linspace(2.5, 8.5, 100)), "amp": 0.1}, 
                {"freq": np.random.choice(np.linspace(16, 64, 100)), "amp": 0.005}] 
    left_edge = np.vstack([(left.shape[1], x) for x in np.arange(left_contours[:,1].min(), left_contours[:,1].max())])
    sine = lambda x, f, a: a*np.sin(2*np.pi * f * x / len(left_edge) + 1.571) + (1-a) # 1.571 is for zero-adjustment (90 degree shift)
    params_ = params()
    sig = lambda x: sum([sine(x, param["freq"], param["amp"]) for param in params_])
    left_edge_dist = np.vstack([(sig(i)*x[0] / len(params_), x[1]) for i, x in enumerate(left_edge)])
    right_edge = np.vstack([(0, x) for x in np.arange(right_contours[:,1].min(), right_contours[:,1].max())])
    sine = lambda x, f, a: a*np.sin(2*np.pi * f * x / len(right_edge) +1.571) + (1-a) # 1.571 is for zero-adjustment (90 degree shift)
    params_ = params()
    sig = lambda x: sum([sine(x, param["freq"], param["amp"])/len(params_) for param in params_])
    right_edge_dist = np.vstack([(800-sig(i)*800, x[1]) for i, x in enumerate(right_edge)])
    
    if plot:
        plt.subplot(121)
        plt.imshow(left, cmap="gray")
    
        plt.scatter(left_contours[:,0], left_contours[:,1])
        plt.scatter(left_edge_dist[:,0], left_edge_dist[:,1], color="r")
    
        plt.subplot(122)
        plt.imshow(right, cmap="gray")
        plt.scatter(right_contours[:,0], right_contours[:,1])
        plt.scatter(right_edge_dist[:,0], right_edge_dist[:,1], color="r")
    src_left = np.vstack([np.vstack(left_contours), left_edge])
    src_right = np.vstack([np.vstack(right_contours), right_edge])
    dst_left = np.vstack([np.vstack(left_contours), left_edge_dist])
    dst_right = np.vstack([np.vstack(right_contours), right_edge_dist])

    return src_left, src_right, dst_left, dst_right

left, right = create_splited_wsi(slide)
src_left, src_right, dst_left, dst_right = get_transform_coordinates(left, right, plot=True)
# +
def create_deformed_image(image: np.ndarray, src_left: np.ndarray, dst_left: np.ndarray, 
                          return_diff: bool = True) -> Union[np.ndarray, Tuple[np.ndarray, np.ndarray]]:
    height, width = image.shape[:2]
    grid_x, grid_y = np.meshgrid(np.arange(width), np.arange(height))    
    # Perform TPS interpolation with sample points
    tps = LinearNDInterpolator(src_left, dst_left)
    orig_coords = np.stack((grid_x.flatten(), grid_y.flatten()),axis=-1)
    deformed_mesh = tps(np.stack((grid_x.flatten(), grid_y.flatten()),axis=-1))
    deformed_mesh[:,0][np.where(np.isnan(deformed_mesh[:,0]))] = orig_coords[:,0][np.where(np.isnan(deformed_mesh[:,0]))]
    deformed_mesh[:,1][np.where(np.isnan(deformed_mesh[:,1]))] = orig_coords[:,1][np.where(np.isnan(deformed_mesh[:,1]))]
    diff = orig_coords.reshape(height, width, 2) - deformed_mesh.reshape(height, width, 2)
    warped = deform_grid(image.T, diff.transpose(2, 1, 0), prefilter=False, mode="constant", cval="255")

    return warped.T if not return_diff else warped.T, diff
    
warped_left, diff = create_deformed_image(left, src_left, dst_left, return_diff = True)
rewarped = deform_grid(warped_left.T, -diff.transpose(2, 1, 0), prefilter=False, mode="constant", cval="255").T

plt.figure(figsize=(14, 20))
plt.subplot(131)
plt.imshow(warped_left, cmap="gray") 
plt.subplot(132)
plt.imshow(left, cmap="gray")
plt.subplot(133)
plt.imshow(rewarped, cmap="gray")
plt.show()

# +
warped_right, diff = create_deformed_image(right, src_right, dst_right, return_diff = True)
rewarped = deform_grid(warped_right.T, -diff.transpose(2, 1, 0), prefilter=False, mode="constant", cval="255").T

plt.figure(figsize=(14, 20))
plt.subplot(131)
plt.imshow(warped_right, cmap="gray") 
plt.subplot(132)
plt.imshow(right, cmap="gray")
plt.subplot(133)
plt.imshow(rewarped, cmap="gray")
plt.show()
# -

plt.imshow(np.hstack([warped_left, warped_right]), cmap="gray")

plt.imshow(img, cmap="gray")

# +
from torch.utils.data import Dataset, DataLoader
from torchvision.transforms import Compose, Resize, ToTensor
from PIL import Image
from pathlib import Path

class WSISplitDataset(Dataset):
    def __init__(self, paths, tfms):
        self.paths = paths
        #self.tfms = tfms

    def __len__(self):
        return len(self.paths)
    
    @staticmethod
    def downsize(img):
        img = Image.fromarray(img)
        width = img.width
        height = img.height
        width = (width // (32 *1)) * 32
        height = (width // (32 *1)) * 32
        return np.asarray(img.resize((height, width)))
        
    def __getitem__(self, idx):
        img = Image.open(self.paths[idx]).convert("RGB").convert("L")
        img = np.asarray(img)
        contours, threshold = get_countours(img)
        left, right = create_splited_wsi(slide)
        left = WSISplitDataset.downsize(left)
        right = WSISplitDataset.downsize(right)
        src_left, src_right, dst_left, dst_right = get_transform_coordinates(left, right, plot=False)

        warped_left, diffl = create_deformed_image(left, src_left, dst_left, return_diff = True)
        warped_right, diffr = create_deformed_image(right, src_right, dst_right, return_diff = True)
        warped_left = torch.tensor(warped_left)
        warped_right = torch.tensor(warped_right)
        warped_left = (warped_left / 255).float()
        warped_right = (warped_right / 255).float()
        left = torch.tensor(left)
        right = torch.tensor(right)
        left = (left / 255).float()
        right = (right / 255).float()
        
        return (torch.tensor(warped_left), torch.tensor(warped_right)), \
               (torch.tensor(diffl), torch.tensor(diffr), torch.tensor(left), torch.tensor(right))

paths = [str(x) for x in Path("../data/tcga").glob("*.tiff")]
train_paths = paths[:int(len(paths)*0.75)]
valid_paths = paths[int(len(paths)*0.75):]
tfms = Compose([Resize(1), ToTensor()])
ds_train = WSISplitDataset(train_paths, tfms)
ds_valid = WSISplitDataset(valid_paths, tfms)
dl_train = DataLoader(ds_train, batch_size=2, drop_last=True)
dl_valid = DataLoader(ds_valid, batch_size=2, drop_last=True)


# -

class Net(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.model = Unet(encoder_name="resnet18", in_channels=1, classes=2, activation='tanh')
        self.metric = torch.nn.MSELoss()
        
    def forward(self, X):
        left, right = X
        self.left = left
        self.right = right
        return self.model(left.unsqueeze(1)), self.model(right.unsqueeze(1))

    def loss(self, X, y):
        loss = 0
        bs = X[0].shape[0]
        for i in range(bs):
            diffl = X[0][i]
            diffr = X[1][i]
            warped_left = self.left[i]
            warped_right = self.right[i]#.permute(1, 2, 0)
            left = y[i][0]#.permute(1, 2, 0)
            right = y[i][1]#.permute(1, 2, 0)
            fl = max(left.shape)
            fr = max(right.shape)

            rewarped_left = tdeform_grid(warped_left.T, -diffl*fl, prefilter=False, mode="constant", cval="1").T.float()
            rewarped_right = tdeform_grid(warped_right.T, -diffr*fr, prefilter=False, mode="constant", cval="1").T.float()
            loss += self.metric(rewarped_left, left) + self.metric(rewarped_right, right)
        return loss
    
    def predict(self, left, right):
        diffs = self.forward((left.unsqueeze(0), right.unsqueeze(0)))
        diffl = diffs[0][0]
        diffr = diffs[0][0]
        fl = max(left.shape)
        fr = max(right.shape)

        rewarpedl = tdeform_grid(left.T, -diffl*fl, prefilter=False, mode="constant", cval="1").T.float()
        rewarpedr = tdeform_grid(right.T, -diffr*fr, prefilter=False, mode="constant", cval="1").T.float()
        return np.hstack([rewarpedl.detach().cpu().numpy(), rewarpedr.detach().cpu().numpy()])


class Net(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.model = Unet(encoder_name="resnet18", in_channels=1, classes=2, activation='tanh')
        self.diff_metric = torch.nn.MSELoss()
        self.image_metric = torch.nn.BCELoss()

        
    def forward(self, X):
        left, right = X
        self.left = left
        self.right = right
        return self.model(left.unsqueeze(1)), self.model(right.unsqueeze(1))

    def diff_loss(self, X, y):
        loss = 0
        bs = X[0].shape[0]
        for i in range(bs):
            diffl = X[0][i]
            diffr = X[1][i]
            left = y[0][i]#.permute(1, 2, 0)
            right = y[1][i]#.permute(1, 2, 0)
            fl = max(left.shape)
            fr = max(right.shape)


            loss += self.diff_metric(diffl*fl, -left.permute(2, 1, 0).float()) + \
                    self.diff_metric(diffr*fr, -right.permute(2, 1, 0).float())
        return loss
    
    def image_loss(self, X, y):
        loss = 0
        bs = X[0].shape[0]
        for i in range(bs):
            diffl = X[0][i]
            diffr = X[1][i]
            warped_left = self.left[i]
            warped_right = self.right[i]#.permute(1, 2, 0)
            left = y[2][i]#.permute(1, 2, 0)
            right = y[3][i]#.permute(1, 2, 0)
            fl = max(left.shape)
            fr = max(right.shape)

            rewarped_left = tdeform_grid(warped_left.T, diffl*fl, prefilter=False, mode="constant", cval="1").T.float()
            rewarped_right = tdeform_grid(warped_right.T, diffr*fr, prefilter=False, mode="constant", cval="1").T.float()
            loss += self.image_metric(rewarped_left, left) + self.metric(rewarped_right, right)
        return loss
    
    def predict(self, left, right):
        diffs = self.forward((left.unsqueeze(0), right.unsqueeze(0)))
        diffl = diffs[0][0]
        diffr = diffs[1][0]
        fl = max(left.shape)
        fr = max(right.shape)
        #diffl[torch.where((diffl*fl).abs() < 25)] = 0
        #diffl[torch.where((diffl*fl).abs() > 500)] = 0


        rewarpedl = tdeform_grid(left.T, diffl*fl, prefilter=False, mode="constant", cval="1").T.float()
        rewarpedr = tdeform_grid(right.T, diffr*fr, prefilter=False, mode="constant", cval="1").T.float()
        return np.hstack([rewarpedl.detach().cpu().numpy(), rewarpedr.detach().cpu().numpy()])

# +
for i in range(2):
    try:
        del model, learn
    except NameError:
        gc.collect()
        torch.cuda.empty_cache()
        
model = Net()
def init_weights(m):
    if isinstance(m, torch.nn.Linear):
        torch.nn.init.constant_(m.weight, 0.001)
        m.bias.data.fill_(0.01)

#model.apply(init_weights)
dls = DataLoaders(dl_train, dl_valid)
learn = Learner(dls, model, loss_func=model.diff_loss)
# -

from fastai.callback.all import EarlyStoppingCallback


learn.fit(150, 1e-4, cbs=[SaveModelCallback(monitor='train_loss', fname='unet_splitloss_20e_1e-4'), EarlyStoppingCallback(patience=15)])

learn.load('unet_splitloss_20e_1e-4')
learn.train()
learn.model = learn.model.cuda()
learn.loss_func = model.mse_loss
learn.fit(150, 1e-5, cbs=[SaveModelCallback(monitor='train_loss', fname='unet_splitloss_20e_1e-4')])

# +
state = model.state_dict()
model = Net()
model.load_state_dict(state)
#model = model.cuda()
d = iter(dl_valid)
#lrw, lr = next(d)
lrw, lr = next(d)

model.eval()

img = model.predict(lrw[0][0], lrw[1][0])
# -


img = (img - img.min()) / (img.max() - img.min())

plt.figure(figsize=(16, 12))
plt.imshow(torch.hstack([lr[2][0], lr[3][0]]), cmap='gray')

plt.figure(figsize=(16, 12))
plt.imshow((img*255).astype('uint8'), cmap='gray')

plt.figure(figsize=(16, 12))
plt.imshow(torch.hstack([lrw[0][0], lrw[1][0]]), cmap='gray')

lr[0][0].sum()


