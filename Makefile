.PHONY: all

IMAGE_VERSION=0.0.1

all: image

image: 
	cp -r deps/pythostitcher docker &&\
	cd docker && docker build --squash -t histo-gui:${IMAGE_VERSION} .
