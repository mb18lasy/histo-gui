import Button from "react-bootstrap/Button";
import { DataTransferService } from "@services/datatransfer-service";
import React, { useRef, useEffect, ChangeEvent } from "react";

export default function ImageUpload({ setInputPath, setProgress, setError }) {
  const ref = useRef();

  const handleClick = (_) => {
    ref.current.click();
  };

  useEffect(() => {
    const handleFileChange = (e: ChangeEvent<HTMLInputElement>) => {
      let file: File;

      file = e.target.files[0];
      const path = file.name;

      DataTransferService.upload(file, path, setProgress)
        .then((response: Promise<any>) => {
          setInputPath(response);
        })
        .catch((error: Promise<any>) => {
          console.log(error);
          setError(true);
        });
    };

    if (ref.current) {
      ref.current.addEventListener("change", handleFileChange);
    }

    return () => {
      if (ref.current) {
        ref.current.removeEventListener("change", handleFileChange);
      }
    };
  }, []);

  return (
    <>
      <Button onClick={handleClick} variant="outline-primary">
        Upload
      </Button>
      <input ref={ref} type="file" style={{ display: "none" }} />
    </>
  );
}
