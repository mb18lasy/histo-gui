import { useEffect } from "react";
import Button from "react-bootstrap/Button";
import { DataTransferService } from "../../services/datatransfer-service";
interface ProcessArgs {
  path: string;
  setOutputPath: any;
  setError: any;
}
export default function ImageProcess({
  path,
  setOutputPath,
  setError,
}: ProcessArgs) {
  const handleClick = (_) => {
    useEffect(() => {
      DataTransferService.preprocess(path)
        .then((response: Promise<any>) => {
          setOutputPath(response);
        })
        .catch((error: Promise<any>) => {
          console.log(error);
          setError(true);
        });
    });
  };

  return (
    <Button onClick={handleClick} variant="outline-danger">
      Process
    </Button>
  );
}
