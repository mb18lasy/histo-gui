import React from "react";
import { useState } from "react";
import { ProgressBar } from "react-bootstrap";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";

import ImageProcess from "./image-process/ImageProcess";
import ImageUpload from "./image-upload/ImageUpload";
import Alert from "react-bootstrap/Alert";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";

interface BarArgs {
  inputPath: string;
  setInputPath: any;
  setOutputPath: any;
}

export default function Bar({
  inputPath,
  setInputPath,
  setOutputPath,
}: BarArgs) {
  const [progress, setProgress] = useState(0);
  const [error, setError] = useState(false);
  return (
    <>
      <Modal show={error} backdrop="static" keyboard={false}>
        <Modal.Header>Upload Error</Modal.Header>
        <Modal.Body>
          Your upload has crashed. Please ensure, the file is a zip archive!
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={() => setError(false)} variant="outline-danger">
            Close
          </Button>
        </Modal.Footer>
      </Modal>
      <Navbar bg="dark" variant="dark">
        <Container>
          <Navbar.Brand href="#home">Menu</Navbar.Brand>
          <Nav className="me-auto">
            <Container>
              <ImageUpload
                setInputPath={setInputPath}
                setProgress={setProgress}
                setError={setError}
              ></ImageUpload>
            </Container>
            <Container>
              <ImageProcess
                path={inputPath}
                setOutputPath={setOutputPath}
                setError={setError}
              ></ImageProcess>
            </Container>
          </Nav>
        </Container>
      </Navbar>
      {progress > 0 ? <ProgressBar now={progress} /> : <div></div>}
    </>
  );
}
