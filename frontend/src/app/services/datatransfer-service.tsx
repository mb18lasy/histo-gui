interface DataTransferService {
  uploadUrl: string;
  downloadUrl: string;
  processUrl: string;
  upload(file: File, path: string, setProgress: any): Promise<any>;
  download(path: string): Promise<any>;
  preprocess(path: string): Promise<any>;
}

export const DataTransferService: DataTransferService = {
  uploadUrl: process.env.NEXT_PUBLIC_UPLOAD_URL
    ? process.env.NEXT_PUBLIC_BASE_URL + process.env.NEXT_PUBLIC_UPLOAD_URL
    : "",
  downloadUrl: process.env.NEXT_PUBLIC_DOWNLOAD_URL
    ? process.env.NEXT_PUBLIC_BASE_URL + process.env.NEXT_PUBLIC_DOWNLOAD_URL
    : "",
  processUrl: process.env.NEXT_PUBLIC_PROCESS_URL
    ? process.env.NEXT_PUBLIC_BASE_URL + process.env.NEXT_PUBLIC_PROCESS_URL
    : "",

  upload(file: File, path: string, setProgress: any): Promise<any> {
    return new Promise((resolve, reject) => {
      const formData = new FormData();
      formData.append("file", file);

      const xhr = new XMLHttpRequest();
      xhr.open("POST", this.uploadUrl + "/" + path, true);
      xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
      xhr.upload.addEventListener("progress", (event) => {
        if (event.lengthComputable) {
          let progress = (event.loaded / event.total) * 100;

          if (progress === 100) {
            setProgress(0);
          } else {
            setProgress(progress);
          }
        }
      });

      xhr.onload = () => {
        if (xhr.status === 200) {
          resolve(xhr.response);
        } else {
          reject(xhr.statusText);
        }
      };

      xhr.onerror = () => {
        reject(xhr.statusText);
      };

      xhr.send(formData);
    });
  },

  download(path: string): Promise<any> {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.open("POST", this.downloadUrl, true);
      xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
      xhr.responseType = "blob";

      xhr.onload = () => {
        if (xhr.status === 200) {
          resolve(xhr.response);
        } else {
          reject(xhr.statusText);
        }
      };

      xhr.onerror = () => {
        reject(xhr.statusText);
      };

      xhr.send(JSON.stringify({ path }));
    });
  },

  preprocess(path: string): Promise<any> {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.open("POST", this.processUrl, true);
      xhr.setRequestHeader("Access-Control-Allow-Origin", "*");

      xhr.onload = () => {
        if (xhr.status === 200) {
          resolve(xhr.response);
        } else {
          reject(xhr.statusText);
        }
      };

      xhr.onerror = () => {
        reject(xhr.statusText);
      };

      xhr.send(JSON.stringify({ path }));
    });
  },
};
