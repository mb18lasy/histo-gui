import ImageDownload from "./image-download/ImageDownload";
import Card from "react-bootstrap/Card";
import Image from "react-bootstrap/Image";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

interface PlotArgs {
  inputPath: string;
  outputPath: string;
}

export default function Plot({ inputPath, outputPath }: PlotArgs) {
  return (
    <>
      <Row>
        <Card>
          <Col>
            <Image src={inputPath} alt="stitched image" />
          </Col>
          <Col>
            <Image src={outputPath} alt="stitched image" />
          </Col>
        </Card>
      </Row>
      <Row>
        <ImageDownload></ImageDownload>
      </Row>
    </>
  );
}
