"use client";
import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import styles from "./page.module.css";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Bar from "./bar/Bar";
import Plot from "./plot/Plot";
import { useState } from "react";
import { TupleType } from "typescript";

export default function Home() {
  const [inputPath, setInputPath] = useState("");
  const [outputPath, setOutputPath] = useState("");
  const [imageIsLoaded, setImageIsLoaded] = useState(false);
  const [imageIsLoading, setImageIsLoading] = useState(false);

  return (
    <>
      <Bar
        inputPath={inputPath}
        setInputPath={setInputPath}
        setOutputPath={setOutputPath}
      ></Bar>
      <main className={styles.main}>
        {inputPath != "" ? (
          <Plot inputPath={inputPath} outputPath={outputPath}></Plot>
        ) : (
          <div className={styles.description}>Please Upload your data!</div>
        )}
      </main>
    </>
  );
}
